var http = require('http');
var httpProxy = require('http-proxy');

var ipaddress = process.env.OPENSHIFT_NODEJS_IP;
var port = process.env.OPENSHIFT_NODEJS_PORT || 8080;

//
// Create a proxy server
//
var proxy = httpProxy.createProxyServer({});

proxy.on('error', function (err, req, res) {
  res.writeHead(500, {
    'Content-Type': 'text/plain'
  });

  res.end('Something went wrong. And we are reporting a custom error message: '+err);
});

//
// Create a server that proxies the request
//
var server = http.createServer(function (req, res) {
    if (req.url === '/') {
        res.end("Hi there!");
    } else {
        req.headers.host = "torn-captcha.appspot.com";
        proxy.web(req, res, {
            target: 'http://torn-captcha.appspot.com'
        });    
    }
});

if (typeof ipaddress === "undefined") {
    console.log("Start server with port:"+port);
    server.listen(port);        
} else {
    console.log("Start server with ip:"+ipaddress+", port:"+port);
    server.listen(port, ipaddress);        
}
